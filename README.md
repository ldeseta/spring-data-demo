# Demo de Spring Data

## Requerimientos
* JDK 8 o superior

## Ejecución
* mvn spring-boot:run

### Pruebas
Algunas URL de prueba:
* http://localhost:8080/api/personas
* http://localhost:8080/api/paises
* http://localhost:8080/api/paises/1
* http://localhost:8080/api/paises/search?nombre=Chile

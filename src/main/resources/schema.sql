drop table if exists persona;
drop table if exists pais;

create table pais(
    id bigint identity primary key,
    nombre varchar(100) not null
);

create table persona(
    id bigint identity primary key,
    nombre varchar(100) not null,
    apellido varchar(100) not null,
    fecha_nacimiento datetime,
    pais_id bigint references pais(id)
);


insert into pais (id, nombre) values (1, 'Argentina');
insert into pais (id, nombre) values (2, 'Brasil');
insert into pais (id, nombre) values (3, 'Chile');
insert into pais (id, nombre) values (4, 'Uruguay');

insert into persona (id, nombre, apellido, pais_id) values (1, 'Kilgore', 'Trout', 1);
insert into persona (id, nombre, apellido, pais_id) values (2, 'Billy', 'Pilgrim', 2);
insert into persona (id, nombre, apellido, pais_id) values (3, 'Dorian', 'Gray', 3);

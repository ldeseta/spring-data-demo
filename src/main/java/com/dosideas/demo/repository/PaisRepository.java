package com.dosideas.demo.repository;

import com.dosideas.demo.domain.Pais;
import org.springframework.data.repository.CrudRepository;

public interface PaisRepository extends CrudRepository<Pais, Long> {

    Pais findByNombre(String nombre);

}

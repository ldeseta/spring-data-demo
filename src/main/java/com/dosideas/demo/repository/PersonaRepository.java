package com.dosideas.demo.repository;

import com.dosideas.demo.domain.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long> {

}

package com.dosideas.demo.controller;

import com.dosideas.demo.domain.Pais;
import com.dosideas.demo.repository.PaisRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PaisController {

    private final PaisRepository paisRepository;

    @GetMapping("/api/paises")
    public Iterable<Pais> findAll() {
        return paisRepository.findAll();
    }

    @GetMapping("/api/paises/{paisId}")
    public Pais findOne(@PathVariable long paisId) {
        return paisRepository.findById(paisId).orElseThrow();
    }

    @GetMapping("/api/paises/search")
    public Pais findOne(@RequestParam String nombre) {
        return paisRepository.findByNombre(nombre);
    }

}

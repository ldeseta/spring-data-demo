package com.dosideas.demo.controller;

import com.dosideas.demo.domain.Persona;
import com.dosideas.demo.repository.PersonaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PersonaController {

    private final PersonaRepository personaRepository;

    @GetMapping("/api/personas")
    public Iterable<Persona> findAll() {
        return personaRepository.findAll();
    }

}
